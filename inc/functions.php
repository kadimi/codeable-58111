<?php

/**
 * Outputs doctype, html, head and body opening tags.
 *
 * @return void
 */
function codeable_58111_tool_opening() {
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<title>Match Colors</title>
		<?php codeable_58111_tool_assets(); ?>
	</head>
	<body>
	<?php
}

/**
 * Outputs html and body closing tags.
 *
 * @return void
 */
function codeable_58111_tool_closing() {
	?>
	</body>
	</html>
	<?php
}

/**
 * Outputs scripts and styles used inside the tool (inside iframe).
 *
 * @return void
 */
function codeable_58111_tool_assets() {
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />
	<link rel="stylesheet" href="https://sindu12jun.github.io/dragula/dist/dragula.min.css" />
 	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://sindu12jun.github.io/dragula/dist/dragula.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.3.6/mobile-detect.min.js"></script>
 	<style>
		body {
			font-size: 16px;
			font-family: 'Open Sans', sans-serif;
		}
		#box,
		#gallery {
			background: #f7f7f7;
			border-radius: .5em;
			margin: 1em;
			min-height: calc( 100px + 2.75em );
			overflow: auto;
		}
		#box {
			border: dashed 3px #ccc;
			max-height: calc( 100px + 2.75em );
			overflow: auto;
		}
		#gallery {
			max-height: calc( 2 * 100px + 2 * 2.75em );
		}
		li.gallery-item {
			display: inline-block;
			height: calc( 100px + 1.25em );
			list-style: none;
			position: relative;
			margin: .75em .5em; 
			width: 100px;
		}
		li.gallery-item > img {
			border-radius: .25em .25em 0 0;
			height: 100px; 
			width: 100px;
		}
		li.gallery-item > figcaption {
			background: #ccc;
			border-radius: 0 0 .25em .25em;
			font-size: .8em;
			height: 1em;
			overflow: hidden;
			padding: .125em;
			text-align: center;
			text-overflow: ellipsis;
			white-space: nowrap;
			width: calc( 100% - .25em );
		}
		#gallery li.gallery-item button {
			display: none;
		}
		li.gallery-item button {
			background: transparent;
			border: none;
			cursor: pointer;
			padding-right: .25em;
			position: absolute;
			right: 0;
			top: 0;
		}
		@media only screen and 	( max-width: 800px ) {
			#box,
			#gallery {
				min-height: calc( 70px + 2.75em );
			}
			#box {
				max-height: calc( 70px + 2.75em );
			}
			#gallery {
				max-height: calc( 2 * 70px + 2 * 2.75em );
			}
			li.gallery-item {
				height: calc( 70px + 1.25em );
				width: 70px;
			}
			li.gallery-item > img {
				height: 70px; 
				width: 70px;
			}
			li.gallery-item > figcaption {
				font-size: .7em;
			}
	</style>
	<script>
		jQuery( document ).ready( function( $ ) {

			var preventScroll = function () {
				$()
					.add( 'body' )
					.add( 'body, .featherlight', window.top.document )
					.css( 'overflow', 'hidden' )
				;
			};

			var allowScroll = function () {
				$()
					.add( 'body' )
					.add( 'body, .featherlight', window.top.document )
					.css( 'overflow', 'auto' )
				;
			};

			var drake = dragula( [ document.getElementById( 'gallery' ), document.getElementById( 'box' ) ], {
				animation: 400,
				direction: 'horizontal',
				copy: function ( el, source ) {
					return source === document.getElementById( 'gallery' )
				},
				moves: function ( el, source, handle, sibling ) {
					if ( document.getElementById( 'gallery' ) === source ) {
						var boxMax      = 8;
						var boxContains = $( '#box .gallery-item' ).length;
						return boxContains < boxMax;
					} else {
						return true;
					}
				},
				accepts: function ( el, target ) {
					return target !== document.getElementById( 'gallery' )
				}
			} );

			/**
			 * Prevent scrolling when interating with the tool, allow otherwise.
			 */
			var md;
			drake.on( 'drag', function() {
				md = new MobileDetect(window.navigator.userAgent);
				if ( ! md.mobile() ) {
					return;
				}
				preventScroll();
			} );
			drake.on( 'dragend', allowScroll );
			$()
				.add('#tool')
				.add('.featherlight, .featherlight-content', window.top.document)
				.on({
					'touchstart': preventScroll,
					'touchend': allowScroll
				})
			;

			/**
			 * Remove color.
			 */
			$( document ).on( 'click tap doubleclick', '#box button', function() {
				$( this ).parent().remove();
			} );

			/**
			 * Change opacity when maximum items are dragged.
			 */
			$( document ).on( 'click tap doubleclick', '#box button', function() {
				$( '#gallery' ).fadeTo( 400, 1);
			} );
			drake.on( 'dragend', function() {
				var boxMax      = 8;
				var boxContains = $( '#box .gallery-item' ).length;
				$( '#gallery' ).fadeTo( 400, boxContains < boxMax ? 1 : .05 );
			} );
		} );
	</script>
	<?php
}

/**
 * Outputs gellery.
 *
 * @return void
 */
function codeable_58111_tool_gallery( $gallery ) {
	echo '<ul id="gallery">';
	foreach ( $gallery as $item ) {
		codeable_58111_tool_gallery_item( $item );
	}
	echo '</ul>';
}

/**
 * Outputs gallery item.
 *
 * @return void
 */
function codeable_58111_tool_gallery_item( $item ) {
	printf( '<li class="gallery-item"><img src="%2$s" title="%1$s"><figcaption>%1$s</figcaption><button>x</button></li>'
		, $item['title']
		, $item['url']
	);
}

/**
 * Outputs box.
 *
 * @return void
 */
function codeable_58111_tool_box() {
	echo '<ul id="box"></ul>';
}

/**
 * Outputs tool.
 *
 * @return void
 */
function codeable_58111_tool( $gallery ) {

	echo '<div id="tool">';

	/**
	 * Output box.
	 */
	codeable_58111_tool_box();

	/**
	 * Output gallery.
	 */
	codeable_58111_tool_gallery( $gallery );

	echo '</div>';
}

/**
 * Callback for the shortcode [match-images id=? content=?].
 *
 * @param  String $post_id Post ID.
 * @param  String $content Content to use for the link.
 * @return String shortcode output.
 */
function codeable_58111_shortcode_match_images( $post_id, $content ) {

	if ( ! $post_id || ! $content ) {
		return '';
	}

	ob_start();
	?>
		<a href="#" data-featherlight="#match-colors" id="match-colors-trigger"><?php echo $content; ?></a>
		<div id="match-colors-wrapper">
		<div id="match-colors">
			<iframe src="<?php echo site_url( 'match-colors/id/' . $post_id ); ?>" frameborder="0"></iframe>
		</div>
		</div>
	<?php
	codeable_58111_shortcode_match_images_assets();
	return ob_get_clean();
}

/**
 * Outputs shortcode scripts and styles.
 *
 * @return void
 */
function codeable_58111_shortcode_match_images_assets() {
	?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.7.6/featherlight.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/featherlight/1.7.6/featherlight.min.css" />
		<style>
			#match-colors-wrapper {
				display: none;
			}
			#match-colors {
			}
			#match-colors iframe {
				height: 500px;
				overflow: hidden;
				width: 90vw;
			}
			@media only screen and ( max-width: 900px ) {
				#match-colors iframe {
					height: 400px;
				}
			}
			.featherlight {
				border-top: solid 5px transparent;
				max-height: 70%;
				overflow: auto;
				padding: 15px;
				top: auto;
			}
		</style>
		<script>

			jQuery( document ).ready( function( $ ) {
				$.featherlight.defaults.beforeOpen = function() {
					$( '#match-colors-trigger' ).hide( 200 );
				};
				$.featherlight.defaults.afterClose = function() {
					$( '#match-colors-trigger' ).show( 200 );
				};
			} );
		</script>
	<?php
}
