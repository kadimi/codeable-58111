<?php

add_action( 'init', function() {
	add_shortcode( 'match-colors', function( $atts ) {
		$args = shortcode_atts( [
			'id' => false,
			'content' => '',
		], $atts );
		return codeable_58111_shortcode_match_images( $args['id'], $args['content'] );
	} );
} );
