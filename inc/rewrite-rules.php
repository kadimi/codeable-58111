<?php

/**
 * Add rule.
 */
add_filter( 'init',function () {
	add_rewrite_rule( 'match-colors/id/(\d+)/?$', 'index.php?pagename=match-colors&post_id=$matches[1]', 'top' );
} );

/**
 * Add query sting variables so that WP recognizes it.
 */
add_filter( 'query_vars', function( $query_vars ) {
	$query_vars[] = 'post_id';
	return $query_vars;
} );
