<?php

/**
 * Produce Page
 */
add_action( 'parse_request', function() {

	global $wp;
	$posts = false;

	/**
	 * Only if in 'match-colors'.
	 */
	$pagename = false;
	if ( array_key_exists( 'pagename', $wp->query_vars ) ) {
		$pagename = $wp->query_vars[ 'pagename' ];
	}
	if ( 'match-colors' !== $pagename ) {
		return;
	}

	/**
	 * Get post ID.
	 */
	$post_id = false;
	if ( array_key_exists( 'post_id', $wp->query_vars ) ) {
		$post_id = $wp->query_vars[ 'post_id' ];
	} else {
		return;
	}

	/**
	 * Get gallery
	 */
	$gallery = get_field( 'gallery', $post_id );
	if ( $gallery && ! is_array( $gallery ) ) {
		return;
	}

	/**
	 * Opening.
	 */
	codeable_58111_tool_opening();

	/**
	 * Output tool.
	 */
	codeable_58111_tool( $gallery );

	/**
	 * Closing.
	 */
	codeable_58111_tool_closing();

	/**
	 * Stop.
	 */
	die();
} );

