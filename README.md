Codeable Project [#58111](https://app.codeable.io/tasks/58111/workroom)



# Installation

The plugin can be installed like any other custom WordPress plugin, ie `Plugins > Add New > Upload`. Make sure you delete any old version before installing a newer one.


# How to display the color matching tool



## With a shortcode

`[match-colors id=ID content=CONTENT]`

Where:

- `ID` is the post ID of the product
- `CONTENT` is the content used for the trigerring link, this could be a text or an image.

Example: `[match-colors id=123 content="Open Color Matching Tool"]` will output [Open Color Matching Tool]().

## With PHP code

Find a spot inside a products loop; for example the `content-single-product.php` WooCommerce template file; and insert this code replacing `CONTENT` with your text or image tag:


```PHP
<?php

global $post;

echo do_shortcode( sprintf( '[match-colors id=%d content=\'%s\']'
    , $post->ID
    , 'CONTENT' // Replace this with some text or an HTML image tag. 
) );

?>
```
